<?php
/**
 * @file
 *   Core class of the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier;

use Drupal\field_value_copier\EntityMapping\EntityCache;
use Drupal\field_value_copier\EntityMapping\EntityMapper;
use Drupal\field_value_copier\EntityMapping\Exceptions\AmbiguousEntityException;
use Drupal\field_value_copier\EntityMapping\Exceptions\NoDestinationEntityException;
use Drupal\field_value_copier\EntityMapping\Exceptions\ValueAlreadySetException;

/**
 * A utility for copying values of a field between entities.
 *
 * Both entities must already have instances of the source and destination
 * fields. Only the value of the source field in the latest revision of the
 * source entity will be copied. The value of the field in that revision is
 * copied to the latest revision of the destination entity.
 */
class FieldValueCopier {
  /**
   * The value was successfully copied.
   */
  const COPY_RESULT_SUCCESS = 1;

  /**
   * The value could not be copied because the entity reference field had no
   * value, so there was no associated record for the operation.
   */
  const COPY_RESULT_FAIL_NO_LINKED = 2;

  /**
   * The value could not be copied because there was no deterministic way to
   * decide how to reconcile multiple source or multiple destination values.
   */
  const COPY_RESULT_FAIL_AMBIGUOUS = 3;

  /**
   * The value could not be copied because the destination entity already has a
   * value for the field in its latest revision.
   */
  const COPY_RESULT_FAIL_EXISTS = 4;

  /**
   * @var string
   */
  protected $srcFieldName;

  /**
   * @var string
   */
  protected $srcEntityType;

  /**
   * @var string
   */
  protected $srcEntityBundle;

  /**
   * @var string
   */
  protected $dstFieldName;

  /**
   * @var string
   */
  protected $dstEntityType;

  /**
   * @var string
   */
  protected $dstEntityBundle;

  /**
   * @var EntityMapper
   */
  protected $entityMapper;

  /**
   * @var bool
   */
  protected $deferredSavesEnabled;

  /**
   * @var EntityCache
   */
  protected $deferredSaveCache;

  /**
   * Constructor for FieldValueCopier.
   */
  public function __construct() {
    $this->deferredSaveCache = new EntityCache();

    $this->setDeferredSavesEnabled(FALSE);
  }

  /**
   * Sets the name of the source and destination fields to the same value.
   *
   * This is useful if both the source and destination entities have instances
   * of the same field base.
   *
   * @param string $fieldName
   *   The machine name of the field that both the source and destination share.
   */
  public function setFieldName($fieldName) {
    $this->setSrcFieldName($fieldName);
    $this->setDstFieldName($fieldName);
  }

  /**
   * Gets the machine name of the field from which values are to be copied.
   *
   * @return string
   */
  public function getSrcFieldName() {
    return $this->srcFieldName;
  }

  /**
   * Sets the machine name of the field from which values are to be copied.
   *
   * @param string $srcFieldName
   */
  public function setSrcFieldName($srcFieldName) {
    _field_value_copier_ensure_arg_not_empty($srcFieldName, '$srcFieldName');

    $this->srcFieldName = $srcFieldName;
  }

  /**
   * Gets the machine name of the field to which values are to be copied.
   *
   * @return string
   */
  public function getDstFieldName() {
    return $this->dstFieldName;
  }

  /**
   * Sets the machine name of the field to which values are to be copied.
   *
   * @param string $dstFieldName
   */
  public function setDstFieldName($dstFieldName) {
    _field_value_copier_ensure_arg_not_empty($dstFieldName, '$dstFieldName');

    $this->dstFieldName = $dstFieldName;
  }

  /**
   * Gets the machine name of the entity type from which values will be copied.
   *
   * @return string
   */
  public function getSrcEntityType() {
    return $this->srcEntityType;
  }

  /**
   * Sets the machine name of the entity type from which values will be copied.
   *
   * @param string $srcEntityType
   */
  public function setSrcEntityType($srcEntityType) {
    _field_value_copier_ensure_arg_not_empty($srcEntityType, '$srcEntityType');

    $this->srcEntityType = $srcEntityType;
  }

  /**
   * Gets the machine name of the bundle in the entity type from which values
   * will be copied.
   *
   * @return string
   */
  public function getSrcEntityBundle() {
    return $this->srcEntityBundle;
  }

  /**
   * Sets the machine name of the bundle in the entity type from which values
   * will be copied.
   *
   * @param string $srcEntityBundle
   */
  public function setSrcEntityBundle($srcEntityBundle) {
    _field_value_copier_ensure_arg_not_empty($srcEntityBundle, '$srcEntityBundle');

    $this->srcEntityBundle = $srcEntityBundle;
  }

  /**
   * Gets the machine name of the entity type to which values will be copied.
   *
   * @return string
   */
  public function getDstEntityType() {
    return $this->dstEntityType;
  }

  /**
   * Sets the machine name of the entity type to which values will be copied.
   *
   * @param string $dstEntityType
   */
  public function setDstEntityType($dstEntityType) {
    _field_value_copier_ensure_arg_not_empty($dstEntityType, '$dstEntityType');

    $this->dstEntityType = $dstEntityType;
  }

  /**
   * Gets the machine name of the bundle in the entity type to which values will
   * be copied.
   *
   * @return string
   */
  public function getDstEntityBundle() {
    return $this->dstEntityBundle;
  }

  /**
   * Sets the machine name of the bundle in the entity type to which values will
   * be copied.
   *
   * @param string $dstEntityBundle
   */
  public function setDstEntityBundle($dstEntityBundle) {
    _field_value_copier_ensure_arg_not_empty($dstEntityBundle, '$dstEntityBundle');

    $this->dstEntityBundle = $dstEntityBundle;
  }

  /**
   * Gets the object used to map source entities to destination entities.
   *
   * @return EntityMapper
   */
  public function getEntityMapper() {
    return $this->entityMapper;
  }

  /**
   * Sets the object used to map source entities to destination entities.
   *
   * @param EntityMapper $entityMapper
   */
  public function setEntityMapper(EntityMapper $entityMapper) {
    _field_value_copier_ensure_arg_not_empty($entityMapper, '$entityMapper');

    $this->entityMapper = $entityMapper;
  }

  /**
   * Gets whether or not "deferred saves" are enabled.
   *
   * When deferred saves are *enabled*, destination entities are not
   * immediately saved during <code>copyValues()</code>. Instead, entities that
   * have been modified are stashed in this object and not saved until a later
   * call is made to <code>saveAllDeferred()</code>. This can be a useful
   * option to improve performance when you are copying multiple fields between
   * the same entities via the same <code>FieldValueCopier</code>.
   *
   * When deferred saves are *disabled*, a destination entity is saved
   * immediately after receiving values during <code>copyValues()</code>, and
   * calls to <code>saveAllDeferred()</code> should have no effect.
   *
   * @return boolean
   */
  public function areDeferredSavesEnabled() {
    return $this->deferredSavesEnabled;
  }

  /**
   * When deferred saves are *enabled*, destination entities are not
   * immediately saved during <code>copyValues()</code>. Instead, entities that
   * have been modified are stashed in this object and not saved until a later
   * call is made to <code>saveAllDeferred()</code>. This can be a useful
   * option to improve performance when you are copying multiple fields between
   * the same entities via the same <code>FieldValueCopier</code>.
   *
   * When deferred saves are *disabled*, a destination entity is saved
   * immediately after receiving values during <code>copyValues()</code>, and
   * calls to <code>saveAllDeferred()</code> should have no effect.
   *
   * If deferred saves are initially enabled and then become disabled, any
   * stashed entities that have not yet been saved must still be saved by
   * calling <code>saveAllDeferred()</code>, or changes will be lost.
   *
   * @param boolean $deferredSavesEnabled
   */
  public function setDeferredSavesEnabled($deferredSavesEnabled) {
    if (!is_bool($deferredSavesEnabled)) {
      throw new \InvalidArgumentException(
        '$deferredSavesEnabled must be either TRUE or FALSE.');
    }

    $this->deferredSavesEnabled = $deferredSavesEnabled;
  }

  /**
   * Convenience function for obtaining the IDs of all source objects.
   *
   * This is frequently used in conjunction with the Drupal Batch API so that
   * source entities can be processed in batches instead of trying to copy
   * values for all of the source entities at once.
   */
  public function getAllSourceIds() {
    $query = new \EntityFieldQuery();

    $srcEntityType   = $this->getSrcEntityType();
    $srcEntityBundle = $this->getSrcEntityBundle();

    $query
      ->entityCondition('entity_type', $srcEntityType)
      ->entityCondition('bundle', $srcEntityBundle);

    $results = $query->execute();

    if (!empty($results[$srcEntityType])) {
      $sourceEntityIds = array_keys($results[$srcEntityType]);
    }
    else {
      $sourceEntityIds = array();
    }

    return $sourceEntityIds;
  }

  /**
   * Performs the field value copy operation.
   *
   * An array of the source entity IDs on which to operate can be optionally
   * provided. If not provided, an attempt will be made to copy values for all
   * entity instances of the source type.
   *
   * @param array $srcEntityIds
   *   An array of source entity IDs containing the values to copy.
   *
   * @return array
   *   An array describing the results of the operation, keyed by source entity
   *   ID. The value of each element is a nested array with the following keys:
   *     - src_rev_id: The unique identifier for the revision from which the
   *       values were copied (if the source entity supports revisions).
   *     - src_field: The name of the field in the source entity from which
   *       values were being copied.
   *     - dst_entity_id: The unique identifier for the entity to which the
   *       values were copied.
   *     - dst_rev_id: The unique identifier for the revision to which the
   *       values were copied (if the destination entity supports revisions).
   *     - dst_field: The name of the field in the destination entity to which
   *       values were being copied.
   *     - result: A value indicating whether or not the value was copied:
   *       - COPY_RESULT_SUCCESS: The value was successfully copied.
   *       - COPY_RESULT_FAIL_NO_LINKED: The value could not be copied because
   *         the entity reference field had no value, so there was no associated
   *         record for the operation.
   *       - COPY_RESULT_FAIL_AMBIGUOUS: The value could not be copied because
   *         there was no deterministic way to decide how to reconcile multiple
   *         source or multiple destination values.
   *       - COPY_RESULT_FAIL_EXISTS: The value could not be copied because the
   *         destination entity already has a value for the field in its latest
   *         revision.
   */
  public function copyValues(array $srcEntityIds = NULL) {
    // Not strictly required, but ensures we're following the contract for
    // entity_load() where it expects FALSE instead of NULL to load all
    // entities.
    if ($srcEntityIds === NULL) {
      $srcEntityIds = FALSE;
    }

    $this->validateArgs();

    $dstEntityType  = $this->getDstEntityType();
    $dstEntityId    = NULL;
    $dstRevisionId  = NULL;
    $dstFieldName   = $this->getDstFieldName();

    $srcEntityType = $this->getSrcEntityType();
    $srcEntities   = entity_load($srcEntityType, $srcEntityIds);
    $srcFieldName  = $this->getSrcFieldName();

    $mapper  = $this->getEntityMapper();
    $results = array();

    foreach ($srcEntities as $srcEntity) {
      list($srcEntityId, $srcRevisionId) =
        entity_extract_ids($srcEntityType, $srcEntity);

      try {
        $dstEntity = $mapper->mapToDestinationEntity($this, $srcEntity);

        $this->populateDestination($srcEntity, $dstEntity);

        list($dstEntityId, $dstRevisionId) =
          entity_extract_ids($dstEntityType, $dstEntity);

        $resultStatus = self::COPY_RESULT_SUCCESS;
      }
      catch (NoDestinationEntityException $ex) {
        $resultStatus = self::COPY_RESULT_FAIL_NO_LINKED;
      }
      catch (AmbiguousEntityException $ex) {
        $resultStatus = self::COPY_RESULT_FAIL_AMBIGUOUS;
      }
      catch (ValueAlreadySetException $ex) {
        $resultStatus = self::COPY_RESULT_FAIL_EXISTS;
      }

      $results[$srcEntityId] = array(
        'src_rev_id'    => $srcRevisionId,
        'src_field'     => $srcFieldName,
        'dst_entity_id' => $dstEntityId,
        'dst_rev_id'    => $dstRevisionId,
        'dst_field'     => $dstFieldName,
        'result'        => $resultStatus,
      );
    }

    return $results;
  }

  /**
   * Saves all entities for which saving was previously deferred.
   *
   * The stash of deferred saves is cleared once all have been saved.
   *
   * If no saves were deferred, this method has no effect.
   */
  public function saveAllDeferred() {
    $deferredSaveCache = $this->deferredSaveCache;
    $deferredSaves     = $deferredSaveCache->getAllByType();

    foreach ($deferredSaves as $entityType => $entities) {
      foreach ($entities as $entity) {
        entity_save($entityType, $entity);
      }
    }

    $deferredSaveCache->clear();
  }

  /**
   * Validates that this object is configured correctly.
   */
  protected function validateArgs() {
    _field_value_copier_ensure_arg_not_empty(
      $this->getSrcFieldName(), 'srcFieldName');

    _field_value_copier_ensure_arg_not_empty(
      $this->getSrcEntityType(), 'srcEntityType');

    _field_value_copier_ensure_arg_not_empty(
      $this->getSrcEntityBundle(), 'srcEntityBundle');

    _field_value_copier_ensure_arg_not_empty(
      $this->getDstFieldName(), 'dstFieldName');

    _field_value_copier_ensure_arg_not_empty(
      $this->getDstEntityType(), 'dstEntityType');

    _field_value_copier_ensure_arg_not_empty(
      $this->getDstEntityBundle(), 'dstEntityBundle');

    $entityMapper = $this->getEntityMapper();

    _field_value_copier_ensure_arg_not_empty($entityMapper, 'entityMapper');

    $this->ensureFieldsExist();
    $entityMapper->validateArgs($this);
  }

  /**
   * Populates the field in the destination with values from the source.
   *
   * @param object $srcEntity
   *   The entity from which field values will be drawn.
   * @param object $dstEntity
   *   A reference to the entity to which field values will be written. If
   *   deferred saves are enabled, this instance may be replaced with an
   *   previously-used instance of the same entity, instead.
   *
   * @throws ValueAlreadySetException
   */
  protected function populateDestination($srcEntity, &$dstEntity) {
    $srcFieldName = $this->getSrcFieldName();

    $dstEntityType = $this->getDstEntityType();
    $dstFieldName  = $this->getDstFieldName();

    $dstEntity = $this->reuseExistingDestination($dstEntityType, $dstEntity);

    if (!empty($dstEntity->{$dstFieldName})) {
      list($dstEntityId) = entity_extract_ids($dstEntityType, $dstEntity);

      throw new ValueAlreadySetException(
        "Destination entity `{$dstEntityId}` of type `{$dstEntityType}` " .
        "already has a value for `{$dstFieldName}`.");
    }

    $dstEntity->{$dstFieldName} = $srcEntity->{$srcFieldName};

    $this->saveOrDeferSaving($dstEntityType, $dstEntity);
  }

  /**
   * Replace a destination entity with an existing instance, if available.
   *
   * This is used to ensure that when saves are being deferred, multiple
   * field updates to the same entity are retained on the same instance of the
   * entity.
   *
   * @param string $dstEntityType
   *   The machine name of the entity type to which values are being copied.
   * @param object $dstEntity
   *   The entity to which values are being copied.
   *
   * @return object
   *   Either the destination object that was provided, if no previous instance
   *   was deferred for saving; or, the prior instance that was deferred for
   *   saving.
   */
  protected function reuseExistingDestination($dstEntityType, $dstEntity) {
    $existingDst =
      $this->deferredSaveCache->getByEntity($dstEntityType, $dstEntity);

    if (!empty($existingDst)) {
      $result = $existingDst;
    }
    else {
      $result = $dstEntity;
    }

    return $result;
  }

  /**
   * Either saves a destination entity immediately, or defers it to save later.
   *
   * This determination is based on whether or not deferred saves are enabled
   * at the time this method is invoked.
   *
   * @param string $dstEntityType
   *   The machine name of the entity type to which values are being copied.
   * @param object $dstEntity
   *   The entity to which values are being copied.
   */
  protected function saveOrDeferSaving($dstEntityType, $dstEntity) {
    if ($this->areDeferredSavesEnabled()) {
      $this->deferredSaveCache->putEntity($dstEntityType, $dstEntity);
    }
    else {
      entity_save($dstEntityType, $dstEntity);
    }
  }

  /**
   * Validates that the fields that this object has been configured with
   * actually exist.
   */
  protected function ensureFieldsExist() {
    _field_value_copier_get_field_instance(
      $this->getSrcFieldName(),
      $this->getSrcEntityType(),
      $this->getSrcEntityBundle());

    _field_value_copier_get_field_instance(
      $this->getDstFieldName(),
      $this->getDstEntityType(),
      $this->getDstEntityBundle());
  }
}
