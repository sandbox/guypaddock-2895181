<?php
/**
 * @file
 *   Exceptions in the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping\Exceptions;

/**
 * Exception thrown when an entity mapper encounters a many-to-one relationship.
 *
 * This exception signals that data cannot be copied because either multiple
 * source entities point to the same destination entity, or a single source
 * entity points to multiple destination entities.
 */
class AmbiguousEntityException extends \Exception {
}
