<?php
/**
 * @file
 *   Exceptions in the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping\Exceptions;

/**
 * Exception thrown when an entity mapper cannot locate the destination entity.
 */
class NoDestinationEntityException extends \Exception {
}
