<?php
/**
 * @file
 *   Entity Mappers for the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping;

use Drupal\field_value_copier\FieldValueCopier;
use Drupal\field_value_copier\EntityMapping\Exceptions\AmbiguousEntityException;
use Drupal\field_value_copier\EntityMapping\Exceptions\NoDestinationEntityException;

/**
 * Interface for classes that locates the entity to which field values from
 * a given source entity should be copied.
 */
interface EntityMapper {
  /**
   * Maps a source entity to the entity to which values should be copied.
   *
   * @param FieldValueCopier $fieldCopier
   *   The copier being used for the operation.
   * @param object $srcEntity
   *   The entity from which values are being copied.
   *
   * @return object
   *   The entity to which values should be copied.
   *
   * @throws \InvalidArgumentException
   *   If the source entity is not of the expected type.
   * @throws NoDestinationEntityException
   *   When there is no destination entity for the given source entity.
   * @throws AmbiguousEntityException
   *   If the destination cannot be determined because either multiple source
   *   entities point to the same destination entity, or a single source
   *   entity points to multiple destination entities.
   */
  public function mapToDestinationEntity(FieldValueCopier $fieldCopier,
                                         $srcEntity);

  /**
   * Validates that this object is configured correctly.
   *
   * @param FieldValueCopier $fieldCopier
   *   The copier that is being used for the operation.
   */
  public function validateArgs(FieldValueCopier $fieldCopier);
}
