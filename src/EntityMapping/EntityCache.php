<?php
/**
 * @file
 *   Entity Caching support for the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping;

/**
 * A generic object for storing an entity for later use.
 *
 * Entities are keyed by their type, bundle, primary key, and revision (if
 * applicable). The value cached can be either the same entity, or a different
 * entity (for caching the result of mapping one entity to another.
 */
class EntityCache {
  /**
   * @var array
   */
  protected $data;

  /**
   * Constructor for EntityCache.
   */
  public function __construct() {
    $this->clear();
  }

  /**
   * Clears all entries from this cache.
   */
  public function clear() {
    $this->data = array();
  }

  /**
   * Exports all of the entries in this cache to an associative array.
   *
   * Each key is the machine name of the entity type, and each value is the
   * array of entities of that type.
   *
   * @return array
   *   An associative array keyed by entity type.
   */
  public function getAllByType() {
    $values = array();

    foreach ($this->data as $entityType => $bundles) {
      $entitiesOfType = array();

      foreach ($bundles as $bundle => $entities) {
        foreach ($entities as $entityId => $revision) {
          foreach ($revision as $revisionId => $entity) {
            $entitiesOfType[] = $entity;
          }
        }
      }

      $values[$entityType] = $entitiesOfType;
    }

    return $values;
  }

  /**
   * Look-up an entity by using another instance of it.
   *
   * The bundle, entity ID, and revision ID of the given instance is used as the
   * look-up key for locating the existing instance in the cache.
   *
   * @param string $entityType
   *   The machine name of the entity type.
   * @param object $lookupEntity
   *   The entity used as a template for the actual entity being located.
   *
   * @return object|NULL
   *   Either the existing entity with the specified entity type, bundle,
   *   entity ID, and revision ID; or, NULL, if no such entity was found.
   */
  public function getByEntity($entityType, $lookupEntity) {
    list($entityId, $revisionId, $bundle) =
      entity_extract_ids($entityType, $lookupEntity);

    return $this->getByKeys($entityType, $bundle, $entityId, $revisionId);
  }

  /**
   * Look-up an entity by its identifying characteristics.
   *
   * @param string $entityType
   *   The machine name of the entity type.
   * @param string $bundle
   *   The machine name of the bundle of the entity type.
   * @param int $entityId
   *   The unique identifier for the entity.
   * @param int $revisionId
   *   The unique identifier for the revision of the entity.
   *
   * @return object|NULL
   *   Either the existing entity with the specified entity type, bundle,
   *   entity ID, and revision ID; or, NULL, if no such entity was found.
   */
  public function getByKeys($entityType, $bundle, $entityId, $revisionId) {
    $resultKey = array($entityType, $bundle, $entityId, $revisionId);

    return drupal_array_get_nested_value($this->data, $resultKey);
  }

  /**
   * Stores an entity in the cache.
   *
   * The bundle, entity ID, and revision ID of the entity is used as its lookup
   * key.
   *
   * @param string $entityType
   *   The machine name of the entity type.
   * @param object $entity
   *   The entity to store.
   */
  public function putEntity($entityType, $entity) {
    $this->putEntityMapping($entityType, $entity, $entity);
  }

  /**
   * Stores the mapping from one entity to another in the cache.
   *
   * The bundle, entity ID, and revision ID of the "key entity" is used as the
   * lookup key for the "value entity".
   *
   * @param string $keyEntityType
   *   The machine name of the entity type for the key entity.
   * @param object $keyEntity
   *   The entity to use to derive a look-up key.
   * @param object $valueEntity
   *   The entity stored under the look-up key.
   */
  public function putEntityMapping($keyEntityType, $keyEntity, $valueEntity) {
    list($entityId, $revisionId, $bundle) =
      entity_extract_ids($keyEntityType, $keyEntity);

    $resultKey = array($keyEntityType, $bundle, $entityId, $revisionId);

    drupal_array_set_nested_value($this->data, $resultKey, $valueEntity);
  }
}
