<?php
/**
 * @file
 *   Entity Mappers for the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping;

use Drupal\field_value_copier\FieldValueCopier;

/**
 * A wrapper around entity mappers that caches lookups.
 *
 * This mapper functions identically to entity mapper it wraps in almost all
 * respects, except that a single instance of the mapper will cache the result
 * of mapping a given source entity to a destination entity, and will
 * immediately return that destination entity when given the same source entity
 * again in the future.
 *
 * This cuts down on database overhead when the same source entities are going
 * to be provided multiple times (for example, when multiple fields are being
 * copied between the same source and destination entities).
 */
class CachingEntityMapperWrapper implements EntityMapper {
  /**
   * @var EntityMapper
   */
  protected $mapper;

  /**
   * @var EntityCache
   */
  protected $resultCache;

  /**
   * Constructor for CachingEntityMapperWrapper.
   *
   * @param EntityMapper $mapper
   *   The mapper that this instance is wrapping.
   */
  public function __construct(EntityMapper $mapper) {
    $this->mapper      = $mapper;
    $this->resultCache = new EntityCache();
  }

  /**
   * @inheritdoc
   *
   * The result of mapping a given source entity to a destination is cached, so
   * subsequent calls with the same source will return the same destination
   * without additional overhead.
   */
  public function mapToDestinationEntity(FieldValueCopier $fieldCopier,
                                         $srcEntity) {
    $resultCache   = $this->resultCache;
    $srcEntityType = $fieldCopier->getSrcEntityType();
    $dstEntity     = $resultCache->getByEntity($srcEntityType, $srcEntity);

    if (empty($dstEntity)) {
      $mapper = $this->mapper;

      $dstEntity = $mapper->mapToDestinationEntity($fieldCopier, $srcEntity);

      $resultCache->putEntityMapping($srcEntityType, $srcEntity, $dstEntity);
    }

    return $dstEntity;
  }

  /**
   * @inheritdoc
   */
  public function validateArgs(FieldValueCopier $fieldCopier) {
    $this->mapper->validateArgs($fieldCopier);
  }
}
