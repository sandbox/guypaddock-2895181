<?php
/**
 * @file
 *   Entity Mappers for the Field Value Copier module.
 *
 *   © 2017 RedBottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\field_value_copier\EntityMapping;

use Drupal\field_value_copier\EntityMapping\Exceptions\AmbiguousEntityException;
use Drupal\field_value_copier\EntityMapping\Exceptions\NoDestinationEntityException;
use Drupal\field_value_copier\FieldValueCopier;

/**
 * A mapper that uses entity reference fields to map source and destination.
 *
 * The two entities (source and destination) must be linked by an entity
 * reference field, which can exist on either the source or destination entity.
 * In addition, there must be a 1:1 relationship between the entities. If an
 * instance of the entity owning the entity reference field has multiple values
 * (i.e. multiple deltas), that instance of the entity cannot be mapped.
 *
 * For example, consider using this module to move a product image field
 * from a Product Display to a Product Entity:
 *
 *   - If each product display points to only a single product variation, then
 *     this mapper can locate every variation that should receive a product
 *     image.
 *
 *   - If any product displays point to more than one product variation, then
 *     this mapper cannot determine which variations should receive the
 *     product image.
 *
 *   - If more than one product display points to the same product variation,
 *     then this mapper cannot determine which product image the variation
 *     should receive.
 */
class ReferenceFieldEntityMapper implements EntityMapper {
  /**
   * The source entity owns the reference field.
   */
  const SRC_OWNS_REF = 1;

  /**
   * The destination entity owns the reference field.
   */
  const DST_OWNS_REF = 2;

  /**
   * @var string
   */
  protected $refFieldName;

  /**
   * @var int
   */
  protected $refFieldOwner;

  /**
   * Constructor for ReferenceFieldEntityMapper.
   *
   * @param string $refFieldName
   *   The name of the entity reference field that links the source and
   *   destination entities.
   * @param int $refFieldOwner
   *   Which entity owns the entity reference field. If not set, defaults to
   *   <code>SRC_OWNS_REF</code>.
   */
  public function __construct($refFieldName  = NULL,
                              $refFieldOwner = self::SRC_OWNS_REF) {
    if (isset($refFieldName)) {
      $this->setRefFieldName($refFieldName);
    }

    $this->setRefFieldOwner($refFieldOwner);
  }

  /**
   * Gets the name of the entity reference field that links the source and
   * destination entities.
   *
   * @return string
   */
  public function getRefFieldName() {
    return $this->refFieldName;
  }

  /**
   * Sets the name of the entity reference field that links the source and
   * destination entities.
   *
   * @param string $refFieldName
   */
  public function setRefFieldName($refFieldName) {
    _field_value_copier_ensure_arg_not_empty($refFieldName, '$refFieldName');

    $this->refFieldName = $refFieldName;
  }

  /**
   * Gets which entity owns the entity reference field.
   *
   * The value can be either:
   *   - SRC_OWNS_REF: The source entity owns the field.
   *   - DST_OWNS_REF: The destination entity owns the field.
   *
   * @return int
   */
  public function getRefFieldOwner() {
    return $this->refFieldOwner;
  }

  /**
   * Sets which entity owns the entity reference field.
   *
   * The value can be either:
   *   - SRC_OWNS_REF: The source entity owns the field.
   *   - DST_OWNS_REF: The destination entity owns the field.
   *
   * @param int $refFieldOwner
   */
  public function setRefFieldOwner($refFieldOwner) {
    $validOwners = array(self::SRC_OWNS_REF, self::DST_OWNS_REF);

    if (!in_array($refFieldOwner, $validOwners)) {
      throw new \InvalidArgumentException(
        'Unexpected $refFieldOwner value: ' . $refFieldOwner);
    }

    $this->refFieldOwner = $refFieldOwner;
  }

  /**
   * @inheritdoc
   */
  public function validateArgs(FieldValueCopier $fieldCopier) {
    $refFieldName = $this->getRefFieldName();

    _field_value_copier_ensure_arg_not_empty($refFieldName, 'refFieldName');

    list($refEntityType, $refBundle) = $this->getRefFieldInfo($fieldCopier);

    // Ensures reference field exists
    _field_value_copier_get_field_instance(
      $refFieldName, $refEntityType, $refBundle);
  }

  /**
   * @inheritdoc
   */
  public function mapToDestinationEntity(FieldValueCopier $fieldCopier,
                                         $srcEntity) {
    list($parentEntityType, $parentBundle) =
      $this->getRefFieldInfo($fieldCopier);

    $refFieldName = $this->getRefFieldName();

    if ($this->getRefFieldOwner() == self::SRC_OWNS_REF) {
      $parentId = $this->getEntityId($parentEntityType, $srcEntity);

      $childEntityType = $fieldCopier->getDstEntityType();

      $destination =
        $this->getChildEntity(
          $parentEntityType,
          $childEntityType,
          $refFieldName,
          $parentId);

      // Not strictly required for migration, but helps to ensure we fail fast
      $this->ensureParentRefIsOneToOne(
        $parentEntityType,
        $parentBundle,
        $childEntityType,
        $destination);
    }
    else {
      $childEntityType = $fieldCopier->getSrcEntityType();
      $childId         = $this->getEntityId($childEntityType, $srcEntity);

      $destination =
        $this->getParentEntity(
          $parentEntityType,
          $parentBundle,
          $refFieldName,
          $childId);

      // Not strictly required for migration, but helps to ensure we fail fast
      $this->ensureChildRefIsOneToOne(
        $parentEntityType,
        $childEntityType,
        $destination);
    }

    return $destination;
  }

  /**
   * Ensures that the reference from a parent back to its child is 1:1 as well.
   *
   * This is invoked as an additional sanity check after we've confirmed that
   * the relationship from the child entity to the parent entity was 1:1, just
   * to make sure that the relationship the other way around is also 1:1.
   *
   * Without this sanity check, if the parent object is the data source, it
   * would be possible to start copying values from the parent object to the
   * child object before realizing that there are actually multiple parents that
   * refer to the same child object. It would eventually be caught when more
   * than one parent tries to copy data to the child object (since we prevent
   * overwrites), but by then we would have copied data from one of the parents.
   *
   * @param string $parentEntityType
   *   The machine name of the entity type containing the entity reference
   *   field.
   * @param string $parentBundle
   *   The machine name of the bundle of the entity containing the entity
   *   reference field.
   * @param string $childEntityType
   *   The machine name of the entity type being referenced by the entity
   *   reference field.
   * @param object $destination
   *   The child object to which data is being written.
   *
   * @throws AmbiguousEntityException
   *   If multiple source point to the same destination entity.
   */
  protected function ensureParentRefIsOneToOne($parentEntityType, $parentBundle,
                                               $childEntityType, $destination) {
    $refFieldName = $this->getRefFieldName();
    $childId      = $this->getEntityId($childEntityType, $destination);

    // This will throw AmbiguousEntityException if the back-ref is not 1:1
    $this->getParentEntity(
      $parentEntityType,
      $parentBundle,
      $refFieldName,
      $childId);
  }

  /**
   * Ensures that the reference from a child back to its parent is 1:1 as well.
   *
   * This is invoked as an additional sanity check after we've confirmed that
   * the relationship from the parent entity to the child entity was 1:1, just
   * to make sure that the relationship the other way around is also 1:1.
   *
   * Without this sanity check, if the child object is the data source, it
   * would be possible to start copying values from the child object to the
   * parent object before realizing that there are actually multiple children
   * that refer to the same parent object. It would eventually be caught when
   * more than one child tries to copy data to the parent object (since we
   * prevent overwrites), but by then we would have copied data from one of the
   * children.
   *
   * @param string $parentEntityType
   *   The machine name of the entity type containing the entity reference
   *   field.
   * @param string $childEntityType
   *   The machine name of the entity type being referenced by the entity
   *   reference field.
   * @param object $destination
   *   The parent object to which data is being written.
   *
   * @throws AmbiguousEntityException
   *   If multiple source point to the same destination entity.
   */
  protected function ensureChildRefIsOneToOne($parentEntityType,
                                              $childEntityType, $destination) {
    $refFieldName = $this->getRefFieldName();
    $parentId     = $this->getEntityId($parentEntityType, $destination);

    // This will throw AmbiguousEntityException if the back-ref is not 1:1
    $this->getChildEntity(
      $parentEntityType,
      $childEntityType,
      $refFieldName,
      $parentId);
  }

  /**
   * Gets the child entity being referenced by a parent entity through a field.
   *
   * @param string $parentEntityType
   *   The machine name of the entity type containing the entity reference
   *   field.
   * @param string $childEntityType
   *   The machine name of the entity type being referenced by the entity
   *   reference field.
   * @param string $fieldName
   *   The machine name of the entity reference field.
   * @param int $parentId
   *   The primary key of the parent entity.
   *
   * @return object
   *   The child entity being referenced by the parent entity through the entity
   *   reference field.
   *
   * @throws AmbiguousEntityException
   *   If the parent entity refers to multiple child entities via the field.
   * @throws NoDestinationEntityException
   *   If the parent entity does not refer to any child entities via the field.
   */
  protected function getChildEntity($parentEntityType, $childEntityType,
                                    $fieldName, $parentId) {
    $loadedEntities = entity_load($parentEntityType, array($parentId));
    $parentEntity   = reset($loadedEntities);

    $valuesForAllLanguages = $parentEntity->{$fieldName};

    if (!empty($valuesForAllLanguages)) {
      $fieldLanguage =
        field_language($parentEntityType, $parentEntity, $fieldName);

      $valuesForLanguage = $valuesForAllLanguages[$fieldLanguage];
    }
    else {
      $valuesForLanguage = array();
    }

    $valueCount = count($valuesForLanguage);

    if ($valueCount == 0) {
      throw new NoDestinationEntityException(
        "Parent entity `{$parentId}` of type `{$parentEntityType}` does not " .
        "reference any child entities via `{$fieldName}`.");
    }
    elseif ($valueCount > 1) {
      throw new AmbiguousEntityException(
        "Parent entity `{$parentId}` of type `{$parentEntityType}` " .
        "references multiple child entities via `{$fieldName}`.");
    }

    $fieldColumn = $this->getRefFieldColumn($fieldName);
    $firstValue  = reset($valuesForLanguage);

    $childId  = $firstValue[$fieldColumn];
    $entities = entity_load($childEntityType, array($childId));

    return reset($entities);
  }

  /**
   * Gets the parent entity referencing a child entity through a field.
   *
   * @param string $parentEntityType
   *   The machine name of the entity containing the entity reference field.
   * @param string $parentBundle
   *   The machine name of the bundle of the entity containing the entity
   *   reference field.
   * @param string $fieldName
   *   The machine name of the entity reference field.
   * @param int $childId
   *   The primary key of the child entity being referenced.
   *
   * @return object
   *   The parent entity that is referencing the child  entity through the
   *   entity reference field.
   *
   * @throws AmbiguousEntityException
   *   If multiple parent entities refer to the child entity via the field.
   * @throws NoDestinationEntityException
   *   If no parent entity was found that refers to the child entity via the
   *   field.
   */
  protected function getParentEntity($parentEntityType, $parentBundle,
                                     $fieldName, $childId) {
    $query = new \EntityFieldQuery();

    $fieldColumn = $this->getRefFieldColumn($fieldName);

    $query
      ->entityCondition('entity_type', $parentEntityType)
      ->entityCondition('bundle', $parentBundle)
      ->fieldCondition($fieldName, $fieldColumn, $childId)
      ->range(0, 2);

    $results = $query->execute();

    if (isset($results[$parentEntityType])) {
      $parentIds = array_keys($results[$parentEntityType]);
    }
    else {
      $parentIds = array();
    }

    $entityCount = count($parentIds);

    if ($entityCount == 0) {
      throw new NoDestinationEntityException(
        "There are no `{$parentEntityType}` entities that reference " .
        "child entity `{$childId}` via `{$fieldName}`.");
    }
    elseif ($entityCount > 1) {
      throw new AmbiguousEntityException(
        "There are multiple `{$parentEntityType}` entities that reference " .
        "child entity `{$childId}` via `{$fieldName}`.");
    }

    $entities = entity_load($parentEntityType, $parentIds);

    return reset($entities);
  }

  /**
   * Gets the database column that stores values for an entity reference field.
   *
   * @param string $fieldName
   *   The machine name of the field for which a column is needed.
   *
   * @return string
   *   The database column that stores the target entity ID.
   */
  protected function getRefFieldColumn($fieldName) {
    $fieldInfo = field_info_field($fieldName);

    if (empty($fieldInfo)) {
      throw new \InvalidArgumentException('Unknown field: ' . $fieldName);
    }

    $columns     = $fieldInfo['columns'];
    $columnNames = array_keys($columns);
    $columnCount = count($columnNames);

    if ($columnCount > 1) {
      throw new \InvalidArgumentException(
        "Entity reference fields should have only one column, but " .
        "`{$fieldName}` has `{$columnCount}` columns).");
    }

    return $columnNames[0];
  }

  /**
   * Gets information about the entity and bundle that owns the reference field.
   *
   * @param FieldValueCopier $fieldCopier
   *   The copier that is being used for the operation.
   *
   * @return array
   *   An indexed array containing the reference entity type and bundle (in that
   *   order).
   */
  protected function getRefFieldInfo(FieldValueCopier $fieldCopier) {
    if ($this->getRefFieldOwner() == self::SRC_OWNS_REF) {
      $refEntityType = $fieldCopier->getSrcEntityType();
      $refBundle     = $fieldCopier->getSrcEntityBundle();
    }
    else {
      $refEntityType = $fieldCopier->getDstEntityType();
      $refBundle     = $fieldCopier->getDstEntityBundle();
    }

    return array($refEntityType, $refBundle);
  }

  /**
   * Gets the primary key / unique identifier for a given entity.
   *
   * @param string $entityType
   *   The machine name of the type of entity.
   * @param object $entity
   *   The entity from which an identifier is to be extracted.
   *
   * @return int
   *   The primary key of the entity.
   */
  protected function getEntityId($entityType, $entity) {
    list($entityId) = entity_extract_ids($entityType, $entity);

    return $entityId;
  }
}
