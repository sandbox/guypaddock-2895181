Field Value Copier
==================
A module for Drupal 7 that exposes an API to make it easier to migrate the
values of an existing field in one entity to a field of the same type on a
different entity.

For example, if you are moving a field from a Product Display (of type `node`)
to a Product Variation (of type `commerce_product`), you would first setup the
product variation with an instance of the field, then use this module to copy
the values of each existing product display to its corresponding product
variation. Once complete, you can then remove the field from the Product
Display.

## Authors & Contributors
- Original Author: Guy Elsmore-Paddock (guy@redbottledesign.com)

## Disclaimer
**BACKUP YOUR DATABASE BEFORE USING THIS MODULE**

We try very hard not to lose data, but any time code is moving data from one
place to another in bulk, data loss can result. You are strongly encouraged to
backup your database before doing any migrations / value copying with this
module. We take no responsibility for any data loss that may result, and cannot
assist you in recovering data if it is lost.

## The Default Entity Mapper: `ReferenceFieldEntityMapper`
By default, Field Value Copier (FVC) ships with `ReferenceFieldEntityMapper` --
an `EntityMapper` that associates a `source` (i.e. the entity that has the
existing values to migrate) to a *destination* (i.e. the entity that needs to
receive the existing values) via an entity reference field between the two
entities. When using FVC with Drupal Commerce, the entity reference field used
is typically the field in the product display that refers to its corresponding
product.

### Requirements
As long as there is a one-to-one relationship between two existing entities via
an entity reference field (or product reference field), you can use the
`ReferenceFieldEntityMapper` to move field values between the two entities. It
does not matter which direction the reference field is pointing (i.e. if the
product display references the product, you can still use that reference field
to move values from the product to the product display). The entities must
already exist -- the purpose of this mapper is simply to shift data from an
existing entity to its corresponding existing entity.

### Assumptions
The `ReferenceFieldEntityMapper` makes the following assumptions without
verification:
- Both the source and destination entity types exist.
- The source and destination fields already exist in both the source and
  destination entity types.
- The source and destination fields are exactly the same field type. The fields
  can share the same field base but do not have to -- the fields can have
  different field bases as long as they are the same type of field.
- There is an entity reference field or product reference field that associates
  the source and destination entities. The field can be on either the source or
  destination -- the direction of the field is not important.

## Basic Sample Code
Here's a basic sample of how you'd use this module from within your own module
code:
```PHP
<?php
  use \Drupal\field_value_copier\FieldValueCopier;
  use \Drupal\field_value_copier\EntityMapping\ReferenceFieldEntityMapper;

  $copyMachine = new FieldValueCopier();

  // We're going to move the product image from the product display to the product
  //
  // In this case, both entities are using the same field base, so we're using
  // setFieldName() instead of setSrcFieldName() and setDstFieldName().
  $copyMachine->setFieldName('field_product_image');

  $copyMachine->setSrcEntityType('node');
  $copyMachine->setSrcEntityBundle('product');

  $copyMachine->setDstEntityType('commerce_product');
  $copyMachine->setDstEntityBundle('product');

  $mapper =
    new ReferenceFieldEntityMapper(
      'field_product',
      ReferenceFieldEntityMapper::SRC_OWNS_REF);

  $copyMachine->setEntityMapper($mapper);

  // Sample: Copy values for only the first two entities.
  // This illustrates some of the methods available to you, in case you want to
  // apply a similar pattern to invoke this through Batch API.
  $sourceIds   = $copyMachine->getAllSourceIds();
  $sampleBatch = array_slice($sourceIds, 0, 2);

  // $results will summarize the results of the operation
  $results = $copyMachine->copyValues($sampleBatch);
?>
```

## Advanced Sample Code (Drupal Update Hook)
If you need to update a lot of records, possibly as part of maintaining a
Drupal distribution, you may want to invoke the migration in an implementation
of
[`hook_update_N()`](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_update_N/7.x)
in your `.install` file.

FVC provides a built-in add on that you can require from your code and then
call, to reduce the amount of code you need to write to perform the migration
during a run of <code>update.php</code>. Here's a sample:

```PHP
<?php
// ... lines omitted ...

function MY_PROFILE_update_7001(array &$sandbox) {
  module_load_include('migrate.inc', 'field_value_copier');

  $src_entity_type   = 'node';
  $src_entity_bundle = 'product';

  $dst_entity_type   = 'commerce_product';
  $dst_entity_bundle = 'product';

  $ref_field_name = 'field_product';

  $ref_field_owner =
    \Drupal\field_value_copier\EntityMapping\ReferenceFieldEntityMapper::SRC_OWNS_REF;

  $copy_machine =
    field_value_copier_migrate_create_copy_machine(
      $src_entity_type,
      $src_entity_bundle,
      $dst_entity_type,
      $dst_entity_bundle,
      $ref_field_name,
      $ref_field_owner);

  // We're going to move the product image from the product display to the
  // product
  $fields_to_copy = array(
    'field_product_image' => 'field_product_image',
  );

  field_value_copier_migrate_run_batch(
    $sandbox, $fields_to_copy, $copy_machine);
}
?>
```

By default, this will process your entities in batches of 10, but this is
customizable. See the inline docs for `field_value_copier_migrate_run_batch()`
for more details. At the end, the user should be provided with a message that
summarizes how many records were successfully migrated vs. how many may have
failed for various reasons. More information is logged to the system Watchdog /
DB log.

## Implementing Your Own Entity Mapper
If your project requires you to handle relationships other than 1:1, or if you
need to be able to create objects on the fly, you can supply your own class
that implements `EntityMapper`.
