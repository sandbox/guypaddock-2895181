<?php
/**
 * @file
 *   Migration add-on functions for the Field Value Copier module.
 *
 *   This file is NOT automatically included by the Field Value Copier module.
 *   If you need functionality from this file in your `.install` file, be sure
 *   to explicitly include it, as follows:
 *
 *     module_load_include('migrate.inc', 'field_value_copier');
 *
 *   © 2014-2017 Red Bottle Design, LLC and Inveniem, LLC. All rights reserved.
 *
 * @author Guy Paddock (guy@redbottledesign.com)
 */

use \Drupal\field_value_copier\EntityMapping\CachingEntityMapperWrapper;
use \Drupal\field_value_copier\EntityMapping\ReferenceFieldEntityMapper;
use \Drupal\field_value_copier\FieldValueCopier;

define('FIVACO_MODULE_NAME', 'field_value_copier');

//==============================================================================
// Public Functions
//==============================================================================
/**
 * Creates an instance of the Field Value Copier for a source and destination.
 *
 * @param string $src_entity_type
 *   The machine name of the source entity type.
 * @param string $src_entity_bundle
 *   The machine name of the source entity bundle.
 * @param string $dst_entity_type
 *   The machine name of the destination entity type.
 * @param string $dst_entity_bundle
 *   The machine name of the destination entity bundle.
 * @param string $ref_field_name
 *   The machine name of the entity reference field linking the entities.
 * @param integer $ref_field_owner
 *   Either ReferenceFieldEntityMapper::SRC_OWNS_REF or
 *   ReferenceFieldEntityMapper::DST_OWNS_REF.
 *
 * @return FieldValueCopier
 */
function field_value_copier_migrate_create_copy_machine(
                                          $src_entity_type, $src_entity_bundle,
                                          $dst_entity_type, $dst_entity_bundle,
                                          $ref_field_name, $ref_field_owner) {
  $copier = new FieldValueCopier();

  $ref_field_mapper =
    new ReferenceFieldEntityMapper($ref_field_name, $ref_field_owner);

  $mapper = new CachingEntityMapperWrapper($ref_field_mapper);

  $copier->setSrcEntityType($src_entity_type);
  $copier->setSrcEntityBundle($src_entity_bundle);

  $copier->setDstEntityType($dst_entity_type);
  $copier->setDstEntityBundle($dst_entity_bundle);

  $copier->setEntityMapper($mapper);

  $copier->setDeferredSavesEnabled(TRUE);

  return $copier;
}

/**
 * Runs the batch of field value migrations.
 *
 * This method is expected to be called from an implementation of
 * hook_update_N() or a Batch API callback. This function will handle the rest!
 *
 * It will automatically copy values for the specified fields from the source
 * entity to the destination entity in batches. The size of the batches is
 * determined by what's passed-in for the <code>$batch_size</code> parameter.
 * The <code>$sandbox</code> array will automatically get updated appropriately
 * as each batch is processed, to ensure that Drupal keeps running the
 * operation until all batches have been processed.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage for the update operation.
 *   values are the destination field names.
 * @param array $fields
 *   An associative array where the keys are the source field names and the
 *   values are the destination field names.
 * @param FieldValueCopier $copier
 *   The field copier being used for the operation.
 * @param int $batch_size
 *   The maximum number of entities to process in a single batch. Defaults to
 *   10.
 *
 * @return NULL|string
 *   <code>NULL</code> if the batch is not yet complete; or, the completion
 *   message with results summary if the batch is complete.
 */
function field_value_copier_migrate_run_batch(array &$sandbox, array $fields,
                                              FieldValueCopier $copier,
                                              $batch_size = 10) {
  if (!isset($sandbox['progress'])) {
    _field_value_copier_migrate_setup_sandbox($sandbox, $copier, $fields);
  }

  $last_batch_results =
    _field_value_copier_migrate_run_next_batch($sandbox, $copier, $batch_size);

  $last_batch_size = count($last_batch_results);

  _field_value_copier_migrate_interpret_results($sandbox, $last_batch_results);

  _field_value_copier_migrate_setup_next_batch(
    $sandbox, $last_batch_size, $batch_size);

  return _field_value_copier_migrate_generate_completion_message($sandbox);
}

//==============================================================================
// Internal Functions
//==============================================================================
/**
 * Sets-up Batch API sandbox storage for a field copy operation.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage for the update operation.
 * @param FieldValueCopier $copier
 *   The field copier being used for the operation.
 * @param array $fields_to_copy
 *   An associative array where the keys are the source field names and the
 *   values are the destination field names.
 */
function _field_value_copier_migrate_setup_sandbox(array &$sandbox,
                                                   FieldValueCopier $copier,
                                                   array $fields_to_copy) {
  $all_src_ids = $copier->getAllSourceIds();

  $total_entities = count($all_src_ids);
  $total_fields   = count($fields_to_copy);

  $sandbox['field_map']       = $fields_to_copy;
  $sandbox['current_src_ids'] = $all_src_ids;
  $sandbox['all_src_ids']     = $all_src_ids;

  $sandbox['progress'] = 0;
  $sandbox['max']      = $total_entities * $total_fields;

  $sandbox['total_fields'] = $total_fields;
}

/**
 * Runs the next batch of field value migrations.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage for the update operation.
 *   values are the destination field names.
 * @param FieldValueCopier $copier
 *   The field copier being used for the operation.
 * @param int $batch_size
 *   The maximum number of entities to process in a single batch.
 *
 * @return array
 *   The results of the batch, as returned by
 *   <code>FieldValueCopier::copyValues()</code>.
 */
function _field_value_copier_migrate_run_next_batch(array &$sandbox,
                                                    FieldValueCopier $copier,
                                                    $batch_size) {
  $fields            = $sandbox['field_map'];
  $remaining_src_ids = $sandbox['current_src_ids'];

  $current_src_ids = array_slice($remaining_src_ids, 0, $batch_size);

  $batch_results = array();

  foreach ($fields as $src_field => $dst_field) {
    $copier->setSrcFieldName($src_field);
    $copier->setDstFieldName($dst_field);

    $field_results = $copier->copyValues($current_src_ids);
    $batch_results = array_merge($batch_results, $field_results);
  }

  $copier->saveAllDeferred();

  return $batch_results;
}

/**
 * Sets-up the field value migration sandbox for the next batch of values.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage. The state of this object
 *   will be updated appropriately.
 * @param int $last_batch_size
 *   The number of records actually processed in the previous batch.
 * @param int $max_batch_size
 *   The maximum number of entities to process in a single batch.
 */
function _field_value_copier_migrate_setup_next_batch(array &$sandbox,
                                                      $last_batch_size,
                                                      $max_batch_size) {
  $remaining_src_ids =& $sandbox['current_src_ids'];

  $max_progress      = $sandbox['max'];
  $current_progress =& $sandbox['progress'];
  $finished         =& $sandbox['#finished'];

  $current_progress += $last_batch_size;

  if ($current_progress >= $max_progress) {
    $finished = TRUE;
  } else {
    $finished = ($current_progress / $max_progress);

    // Shift product IDs past what we just processed
    $remaining_src_ids = array_slice($remaining_src_ids, $max_batch_size);
  }
}

/**
 * Updates counters and logs errors from results of a field value copy batch.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage. The state of this object
 *   will be updated appropriately.
 * @param array $results
 *   The results of the batch, as returned by
 *   <code>FieldValueCopier::copyValues()</code>.
 */
function _field_value_copier_migrate_interpret_results(array &$sandbox,
                                                       array $results) {
  if (!isset($sandbox['result_counters'])) {
    $sandbox['result_counters'] = array(
      'success'         => 0,
      'fail_no_linked'  => 0,
      'fail_ambiguous'  => 0,
      'fail_exists'     => 0,
    );
  }

  $counters =& $sandbox['result_counters'];

  foreach ($results as $source_id => $result) {
    $error = _field_value_copier_migrate_interpret_result($result, $counters);

    $src_field = $result['src_field'];
    $dst_field = $result['dst_field'];

    if (isset($error)) {
      watchdog(
        FIVACO_MODULE_NAME,
        'Failed to migrate %src_field from entity %entity_id to %dst_field: @error',
        array(
          '%src_field' => $src_field,
          '%entity_id' => $source_id,
          '%dst_field' => $dst_field,
          '@error'     => $error,
        ),
        WATCHDOG_ERROR);
    }
  }
}

/**
 * Applies counter updates and generates the error message (if any) for a
 * field value copy result.
 *
 * @param array $result
 *   A result from the batch, as returned by
 *   <code>FieldValueCopier::copyValues()</code>.
 * @param array $counters
 *   A reference to the success and failure counters being used to track the
 *   overall operation progress.
 *
 * @return NULL|string
 *   Either <code>NULL</code>, if the result was successful; or, an appropriate
 *   error message if result was a failure.
 */
function _field_value_copier_migrate_interpret_result(array $result,
                                                      array &$counters) {
  $error = NULL;

  switch ($result['result']) {
    case FieldValueCopier::COPY_RESULT_SUCCESS:
      ++$counters['success'];
      break;

    case FieldValueCopier::COPY_RESULT_FAIL_NO_LINKED:
      ++$counters['fail_no_linked'];
      $error = t('There was no linked entity to which the data could be migrated.');
      break;

    case FieldValueCopier::COPY_RESULT_FAIL_AMBIGUOUS:
      ++$counters['fail_ambiguous'];
      $error = t('There was not a one-to-one relationship between source and destination.');
      break;

    case FieldValueCopier::COPY_RESULT_FAIL_EXISTS:
      ++$counters['fail_exists'];
      $error = t('The destination entity already had a value for the field.');
      break;
  }

  return $error;
}

/**
 * Generates the message displayed at the completion of the field value
 * migration.
 *
 * A message is only returned if the batch is complete. The message is also
 * logged to watchdog, in case it does not get returned to the user in the
 * migration.
 *
 * @param array $sandbox
 *   A reference to the Batch API sandbox storage for the operation.
 *
 * @return NULL|string
 *   <code>NULL</code> if the batch is not yet complete; or, the completion
 *   message with results summary if the batch is complete.
 */
function _field_value_copier_migrate_generate_completion_message(
                                                              array &$sandbox) {
  if ($sandbox['#finished'] === TRUE) {
    $field_count  = $sandbox['total_fields'];
    $counters     = $sandbox['result_counters'];

    $results_summary = '<ul>';

    foreach ($counters as $name => $value) {
      $results_summary .= '<li>';

      $results_summary .=
        t('@name: @value',
          array(
            '@name'   => $name,
            '@value'  => $value,
          ));

      $results_summary .= '</li>';
    }

    $results_summary .= '</ul>';

    watchdog(
      FIVACO_MODULE_NAME,
      '@field_count fields migrated: !results_summary',
      array(
        '@field_count'     => $field_count,
        '!results_summary' => $results_summary,
      ),
      WATCHDOG_INFO);

    $result =
      t('@field_count fields migrated: !results_summary',
        array(
          '@field_count'     => $field_count,
          '!results_summary' => $results_summary,
        ));
  }
  else {
    $result = NULL;
  }

  return $result;
}
